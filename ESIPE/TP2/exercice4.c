#include <stdio.h> 

void recursive(int n){
    if(n <= 0){
        return;
    }
    printf("%d ",n);
    recursive(n-1);
    printf("%d ",n);
} 

int main(int argc, char* argv[]){
 recursive(10);
 return 0;
}



